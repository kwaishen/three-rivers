package com.example.threerivers;

import com.example.threerivers.entity.AccountBalance;
import com.example.threerivers.repository.AccountBalanceDao;
import com.example.threerivers.service.AccountBalanceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SpringBootTest
class ThreeRiversApplicationTests {

    @Autowired
    private AccountBalanceService accountBalanceService;
    @Autowired
    private AccountBalanceDao accountBalanceDao;

    @Test
    void contextLoads() {
    }

    @Test
    void testMongo() {
        AccountBalance newAccount = new AccountBalance();
        newAccount.setBalance(BigDecimal.valueOf(99.99));
        newAccount.setAccountName("Steve Shen");
//        newAccount.setLastUpdateTimestamp(new Timestamp(System.currentTimeMillis()));
        newAccount.setLastUpdateTimestamp("1612593994");
        accountBalanceService.saveDirect(newAccount);

//        AccountBalance lucy_guo = accountBalanceService.getBalanceBy("Lucy Guo");
//        System.out.println(lucy_guo);
    }

    @Test
    void deleteRecord() {
        accountBalanceDao.deleteAll();;
    }

}
