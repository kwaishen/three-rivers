package com.example.threerivers;

import com.example.threerivers.util.DateUtil;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class DateUtilTest {

    @Test
    void testGetFirstDateOfXMonthAgo() {
        LocalDate now = LocalDate.of(2021, 2,7);
        int monthAgo = 1;
        LocalDate firstDateOfXMonthAgo = DateUtil.getDateOf1stDayXMonthAgo(now, monthAgo);
        LocalDate lastDayOfXMonthAgo = DateUtil.getDateOfLastDayXMonthAgo(now, monthAgo);

        Assert.assertEquals(1, firstDateOfXMonthAgo.getMonthValue());
        Assert.assertEquals(31, lastDayOfXMonthAgo.getDayOfMonth());
    }

}
