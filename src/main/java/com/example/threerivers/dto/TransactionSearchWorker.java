package com.example.threerivers.dto;

import com.example.threerivers.entity.Transaction;

import java.util.List;

@FunctionalInterface
public interface TransactionSearchWorker {

    List<Transaction> search(SearchCriteria searchCriteria);
}
