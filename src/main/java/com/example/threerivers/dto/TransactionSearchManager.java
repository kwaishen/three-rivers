package com.example.threerivers.dto;

import com.example.threerivers.entity.Transaction;
import com.example.threerivers.entity.TransactionSearchTypeEnum;
import com.example.threerivers.service.TransactionService;
import com.example.threerivers.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

@Component
public class TransactionSearchManager {

    private HashMap<String, TransactionSearchWorker> hashTable;

    @Autowired
    private TransactionService transactionService;

    public TransactionSearchManager() {
        hashTable = new HashMap<>();
        hashTable.put(TransactionSearchTypeEnum.TODAY.getType(), getTodayWorker());
        hashTable.put(TransactionSearchTypeEnum.LAST_7_DAY.getType(), getLast7DayWorker());
        hashTable.put(TransactionSearchTypeEnum.LAST_MONTH.getType(), getLastMonthDayWorker());
        hashTable.put(TransactionSearchTypeEnum.DATE_RANGE.getType(), getDateRangeWorker());
        hashTable.put(TransactionSearchTypeEnum.TRANSACTION_TYPE.getType(), getTransactionTypeWorker());
    }

    public List<Transaction> searchTransaction(SearchCriteria searchCriteria) {
        TransactionSearchWorker worker = this.hashTable.getOrDefault(searchCriteria.getType(), this.defaultWorker());
        return worker.search(searchCriteria);
    }


    private TransactionSearchWorker getTodayWorker() {
        Calendar today = Calendar.getInstance();
        Calendar beginningOfTheDay = new GregorianCalendar(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
                today.get(Calendar.DAY_OF_MONTH), 0,0,0);
        return (searchCriteria -> transactionService.getTransactionsByRange(beginningOfTheDay.getTime(), today.getTime()));
    }

    private TransactionSearchWorker getLast7DayWorker() {
        long DAY_IN_MS = DateUtil.MILLISECOND_PER_SECOND * DateUtil.SECOND_PER_MINUTE *
                DateUtil.MINUTE_PER_HOUR * DateUtil.HOUR_PER_DAY;
        Date sevenDayBefore = new Date(System.currentTimeMillis() - (7 * DAY_IN_MS));
        return (searchCriteria -> transactionService.getTransactionsByRange(sevenDayBefore, Calendar.getInstance().getTime()));

    }

    private TransactionSearchWorker getLastMonthDayWorker() {
        // get last month
        LocalDate now = LocalDate.now();
        // fist day of last month and last day of last month
        LocalDate start = DateUtil.getDateOf1stDayXMonthAgo(now, 1);
        LocalDate end = DateUtil.getDateOfLastDayXMonthAgo(now, 1);
        // convert to date
        Date startDate = Date.from(start.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(end.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return (searchCriteria -> transactionService.getTransactionsByRange(startDate, endDate));
    }

    private TransactionSearchWorker getDateRangeWorker() {
        return (searchCriteria -> transactionService.getTransactionsByRange(searchCriteria.getStartDate(), searchCriteria.getEndDate()));
    }

    private TransactionSearchWorker getTransactionTypeWorker() {
        return (searchCriteria -> transactionService.getTransactionsByType(searchCriteria.getTransactionType()));
    }

    private TransactionSearchWorker defaultWorker() {
        return (searchCriteria -> new ArrayList<>());
    }
}
