package com.example.threerivers.util;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.YearMonth;

@Component
public class DateUtil {
    public static int MILLISECOND_PER_SECOND = 1000;
    public static int SECOND_PER_MINUTE = 60;
    public static int MINUTE_PER_HOUR = 60;
    public static int HOUR_PER_DAY = 24;

    public static LocalDate getDateOf1stDayXMonthAgo(LocalDate date, int xMonthBefore) {
        // get last month
        LocalDate lastMonthDate = date.minusMonths(xMonthBefore);
        // fist day of last month and last day of last month
        return YearMonth.of(lastMonthDate.getYear(), lastMonthDate.getMonth()).atDay(1);
    }

    public static LocalDate getDateOfLastDayXMonthAgo(LocalDate date, int xMonthBefore) {
        // get last month
        LocalDate lastMonthDate = date.minusMonths(xMonthBefore);
        // fist day of last month and last day of last month
        return YearMonth.of(lastMonthDate.getYear(), lastMonthDate.getMonth()).atEndOfMonth();
    }
}
