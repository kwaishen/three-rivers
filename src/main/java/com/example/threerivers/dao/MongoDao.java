package com.example.threerivers.dao;

import com.example.threerivers.entity.AccountBalance;

public interface MongoDao {
    AccountBalance getAccountBalanceByAccountName(String accountName);

    AccountBalance saveAccountBalance(AccountBalance accountBalance);
}
