package com.example.threerivers.entity;

public enum TransactionSearchTypeEnum {

    TODAY("today"),
    LAST_7_DAY("last_seven"),
    LAST_MONTH("last_month"),
    DATE_RANGE("date_range"),
    TRANSACTION_TYPE("trans_type")
    ;

    private String type;

    TransactionSearchTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
