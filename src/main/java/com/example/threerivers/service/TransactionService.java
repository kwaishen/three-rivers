package com.example.threerivers.service;

import com.example.threerivers.entity.Transaction;

import java.util.Date;
import java.util.List;

public interface TransactionService {

    List<Transaction> getTransactionsByRange(Date from, Date to);

    List<Transaction> getTransactionsByType(String type);
}
