package com.example.threerivers.service;

import com.example.threerivers.entity.AccountBalance;
import com.example.threerivers.entity.Transaction;

import java.math.BigDecimal;
import java.util.List;

public interface AccountBalanceService {

    List<AccountBalance> getAll();

    AccountBalance getBalanceBy(String accountName);

    AccountBalance saveDirect(AccountBalance accountBalance);

    AccountBalance deposit(Transaction transaction);

    AccountBalance withdraw(Transaction transaction);

}
