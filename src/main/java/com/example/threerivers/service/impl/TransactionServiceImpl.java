package com.example.threerivers.service.impl;

import com.example.threerivers.entity.AccountBalance;
import com.example.threerivers.entity.Transaction;
import com.example.threerivers.repository.AccountBalanceDao;
import com.example.threerivers.repository.TransactionRepository;
import com.example.threerivers.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private AccountBalanceDao accountBalanceDao;

    @KafkaListener(topics = "Transaction", groupId = "group_id", containerFactory = "transactionKafkaListenerFactory")
    public void consume(Transaction transaction) {
        System.out.println("This is a transaction from Kafka Producer " + transaction);
        // check if the account exist

        // deposit
        transaction.setTimestamp(new Timestamp(System.currentTimeMillis()));
        transactionRepository.save(transaction);

        // get current balance
        AccountBalance prevBalance = accountBalanceDao.findByAccountName(transaction.getAccountNumber());
        BigDecimal newBalance = prevBalance.getBalance().add(transaction.getAmount());

        // update previous balance
        prevBalance.setLastUpdateTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        prevBalance.setBalance(newBalance);
        accountBalanceDao.save(prevBalance);
    }

    @Override
    public List<Transaction> getTransactionsByRange(Date from, Date to) {
        Timestamp start = new Timestamp(from.getTime());
        Timestamp end = new Timestamp(to.getTime());
        return transactionRepository.findAllByTimestampBetween(start, end);
    }

    @Override
    public List<Transaction> getTransactionsByType(String type) {
        return transactionRepository.findAllByType(type);
    }
}
