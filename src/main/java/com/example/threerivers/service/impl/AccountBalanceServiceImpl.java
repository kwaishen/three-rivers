package com.example.threerivers.service.impl;

import com.example.threerivers.entity.AccountBalance;
import com.example.threerivers.entity.Transaction;
import com.example.threerivers.repository.AccountBalanceDao;
import com.example.threerivers.repository.TransactionRepository;
import com.example.threerivers.service.AccountBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class AccountBalanceServiceImpl implements AccountBalanceService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountBalanceDao accountBalanceDao;

    @Override
    public List<AccountBalance> getAll() {
        return accountBalanceDao.findAll();
    }

    @Override
    public AccountBalance getBalanceBy(String accountName) {
        return accountBalanceDao.findByAccountName(accountName);
    }

    @Override
    public AccountBalance saveDirect(AccountBalance accountBalance) {
        return accountBalanceDao.save(accountBalance);
    }

    @Override
    public AccountBalance deposit(Transaction transaction) {
        // check if the account exist

        // deposit
        transaction.setTimestamp(new Timestamp(System.currentTimeMillis()));
        transactionRepository.save(transaction);

        // get current balance
        AccountBalance prevBalance = this.getBalanceBy(transaction.getAccountNumber());
        BigDecimal newBalance = prevBalance.getBalance().add(transaction.getAmount());

        // update previous balance
//        prevBalance.setLastUpdateTimestamp(new Timestamp(System.currentTimeMillis()));
        prevBalance.setBalance(newBalance);
        accountBalanceDao.save(prevBalance);

        return prevBalance;
    }

    @Override
    public AccountBalance withdraw(Transaction transaction) {
        // check if the account exist

        // deposit
        transaction.setTimestamp(new Timestamp(System.currentTimeMillis()));
        transactionRepository.save(transaction);

        // get current balance
        AccountBalance prevBalance = this.getBalanceBy(transaction.getAccountNumber());
        BigDecimal newBalance = prevBalance.getBalance().subtract(transaction.getAmount());

        // update previous balance
//        prevBalance.setLastUpdateTimestamp(new Timestamp(System.currentTimeMillis()));
        prevBalance.setBalance(newBalance);
        accountBalanceDao.save(prevBalance);

        return prevBalance;
    }
}
