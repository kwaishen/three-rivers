package com.example.threerivers.repository;

import com.example.threerivers.entity.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    List<Transaction> findAllByType(String type);

    List<Transaction> findAllByTimestampBetween(Timestamp start, Timestamp end);

}
