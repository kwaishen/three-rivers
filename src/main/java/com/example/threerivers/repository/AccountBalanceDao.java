package com.example.threerivers.repository;

import com.example.threerivers.entity.AccountBalance;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountBalanceDao extends MongoRepository<AccountBalance, String> {

    AccountBalance findByAccountName(String accountName);
}
