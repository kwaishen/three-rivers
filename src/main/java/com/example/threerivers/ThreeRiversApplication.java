package com.example.threerivers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = {"com.example.threerivers.repository", "com.example.threerivers.dao"})
@SpringBootApplication
public class ThreeRiversApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThreeRiversApplication.class, args);
    }

}
