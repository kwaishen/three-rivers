package com.example.threerivers.controller;


import com.example.threerivers.dto.SearchCriteria;
import com.example.threerivers.dto.TransactionSearchManager;
import com.example.threerivers.entity.AccountBalance;
import com.example.threerivers.entity.Transaction;
import com.example.threerivers.service.AccountBalanceService;
import com.example.threerivers.service.TransactionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

    @Autowired
    private AccountBalanceService accountBalanceService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private TransactionSearchManager transactionSearchManager;

    @GetMapping(value = "/balance/all")
    public ResponseEntity<Object> getAllBalance(){
        List<AccountBalance> all = accountBalanceService.getAll();
        return ResponseEntity.ok().body(all);
    }

    @GetMapping(value = "/balance")
    public ResponseEntity<Object> getAccountBalanceByAccountName(@RequestParam String accountName) {
        if (StringUtils.isBlank(accountName)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("error", "Invalid Account Name");
            return ResponseEntity.badRequest().body(map);
        }

        AccountBalance balanceBy = accountBalanceService.getBalanceBy(accountName);
        return ResponseEntity.ok(balanceBy);
    }

    @PostMapping(value = "/deposit")
    public ResponseEntity<Object> moveMoney(@RequestBody Transaction transaction) {
        if (transaction.getAmount().doubleValue() <= 0) {
            HashMap<String, String> map = new HashMap<>();
            map.put("error", "Invalid Amount");
            return ResponseEntity.badRequest().body(map);
        }
        AccountBalance newBalance = accountBalanceService.deposit(transaction);
        return ResponseEntity.ok().body(newBalance);
    }

    @PostMapping(value = "/withdraw")
    public ResponseEntity<Object> withdraw(@RequestBody Transaction transaction) {
        if (transaction.getAmount().doubleValue() <= 0) {
            return ResponseEntity.badRequest().body("Amount must be positive");
        }
        AccountBalance newBalance = accountBalanceService.withdraw(transaction);
        return ResponseEntity.ok().body(newBalance);
    }

    @PostMapping(value = "/search")
    public ResponseEntity<Object> getTransactionByType(@RequestBody SearchCriteria searchCriteria) {
        if (!StringUtils.isBlank(searchCriteria.getType())) {
            List<Transaction> transactions = transactionSearchManager.searchTransaction(searchCriteria);
            return ResponseEntity.ok().body(transactions);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("error", "Invalid search criteria");
        return ResponseEntity.badRequest().body(map);

    }
}
